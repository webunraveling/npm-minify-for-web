A quick tool for compiling and minifying source files. You can do a quick install to compile source files and then place the minified versions into your project. If you really want, you could use this within a project.

# Installation
Assuming NPM and Git are already installed just clone this repo and use NPM to install all deps.
```
$ git clone https://gitlab.com/webunraveling/npm-minify-for-web.git
$ cd npm-minify-for-web
$ npm install
```

# Usage
All source files are store in the main directory under the **js** and **scss** directories. They are compiled to the **dist** directory.

`npm run compile-css` compiles all Sass as a non-minified CSS file.  
`npm run minify-css` minifies the **style.css** file in the **dist** directory.  
`npm run minify-js` minifies all JavaScript files in the **js** directory into one file in the **dist** directory.  
`npm run all` runs all of the above commands

# License

There isn't much here for a license but just so you know, you can do whatever you want with it.

## MIT License

Copyright 2022 Jason Raveling

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
